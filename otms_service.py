import sys
import os
import win32service
import win32serviceutil
import win32event
import servicemanager
import win32api
import otms_proc_pause
import traceback
import configparser
import logging
import os
from datetime import datetime
import random

class OtmsService(win32serviceutil.ServiceFramework):

    _svc_name_ = 'MOTMSSVC'
    _svc_display_name_ = 'MyOTMSService'



    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.haltEvent = win32event.CreateEvent(None, 0,0, None)
        self.is_running = False


        if getattr(sys, 'frozen', False):
            #pyinstaller로 패키징 된 경우
            self.cur_path = os.path.dirname(sys.executable)
        elif __file__:
            # 일반 소스의 경우
            self.cur_path = os.path.dirname(os.path.abspath(__file__))

        config = configparser.ConfigParser()
        config.read(self.cur_path + '/opp.ini')

        str_log_level = config['GENERAL']['LOGLEVEL']
        self._timeout = int(config['GENERAL']['CHECK_TERM'])
        log_level = 0
        if str_log_level == 'DEBUG':
            log_level = logging.DEBUG
        elif str_log_level == 'ERROR':
            log_level = logging.ERROR
        elif str_log_level == 'WARN':
            log_level = logging.WARN
        else:
            log_level = logging.INFO

        logging.basicConfig(filename=self.cur_path + '/opp.log', format='%(asctime)s [%(levelname)-5s] %(message)s',
                            level=log_level)

        logging.info('SERVICE initilized.')
        logging.info('LOG : ' + self.cur_path + "/opp.log")
        logging.info("CHECK TERM : %d seconds" % (self._timeout / 1000 ))

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.is_running = False
        win32event.SetEvent(self.haltEvent)

    def SvcDoRun(self):
        print("run")

        #f = open('c:/xxx.dat','w')
        #f.write("XXX\n")


        self.is_running = True
        #f.write("is_running : %s\n" % self.is_running )


        #f.write("Let's start.\n")
        #f.flush()

        logging.info("[SERVICE] Let's start.")

        is_shutdown = False

        while self.is_running:
            rc = win32event.WaitForSingleObject(self.haltEvent, self._timeout)

            if rc == win32event.WAIT_OBJECT_0:
                # Stop signal
                #f.write("Stop signal.")
                #f.flush()
                logging.info("[SERVICE] Stop Signal")
                break
            else :
                ret = None
                #f.write("CURRNT DIR : %s \n" % os.getcwd())
                #f.write("Run check_otms function\n")
                #f.flush()
                try:
                    logging.info("[SERVICE] RUN check_otms function.")
                    ret =otms_proc_pause.check_otms()
                except:
                    #f.write(traceback.format_exc())
                    #f.flush()
                    logging.warning("[SERVICE] %s" % traceback.format_exc())

                #f.write("Run check_otms function RET:%s\n" % ret)
                #f.flush()
                logging.warning("[SERVICE] RETURN of check_otms function is : %s" % ret)

                if is_shutdown == False :
                    logging.info("[SHUTDOWN] Check shutdown.")
                    str_now_hour = datetime.now().strftime('%H')
                    #오후 9시 이후라면,
                    if str_now_hour >= '21':
                        after_sec = random.randrange(1800, 7200)
                        logging.info("[SHUTDOWN] My PC will be shut down after %d seconds." % after_sec)
                        os.system('shutdown /s /f /t %d' % after_sec)

                        is_shutdown = True







        #f.close()


def ctrlHandler(strlType):
    return True

if __name__=='__main__':

    #아래 소스 로직은 잘 모른다.
    #서비스로 동작하게 하기 위한 소스

    if getattr(sys, 'frozen', False):
        if len(sys.argv) == 1:
            #pyinstaller로 패키징하여, 서비스로 동작하게 하는 로직
            servicemanager.Initialize()
            servicemanager.PrepareToHostSingle(OtmsService)
            servicemanager.StartServiceCtrlDispatcher()
        else:
            win32api.SetConsoleCtrlHandler(ctrlHandler, True)
            win32serviceutil.HandleCommandLine(OtmsService)
    else:
        # .py 소스를 이용하여 서비스로 동작하게 하는 로직
        win32api.SetConsoleCtrlHandler(ctrlHandler,True)
        win32serviceutil.HandleCommandLine(OtmsService)




