import psutil
import configparser
import logging
import os
import sys
from datetime import datetime


def check_otms():
    cur_path = ''

    if getattr(sys, 'frozen', False):
        cur_path = os.path.dirname(sys.executable)
    elif __file__:
        cur_path = os.path.dirname(os.path.abspath(__file__))

    #f = open("c:/yyy.log","w+")
    #f.write(cur_path+"\n")
    #f.write("INI PATH : " + cur_path+'/opp.ini\n')


    config = configparser.ConfigParser()
    config.read(cur_path+'/opp.ini')

    str_log_level = config['GENERAL']['LOGLEVEL']

    str_kill_start_time = config['GENERAL']['KILL_START_TIME']
    str_kill_end_time = config['GENERAL']['KILL_END_TIME']
    #f.write("STEP1\n")
    log_level = 0
    if str_log_level == 'DEBUG':
        log_level = logging.DEBUG
    elif str_log_level =='ERROR':
        log_level = logging.ERROR
    elif str_log_level =='WARN':
        log_level = logging.WARN
    else:
        log_level = logging.INFO

    #f.write("STEP2\n")
    #f.write(cur_path+'/opp.log\n')
    logging.basicConfig(filename=cur_path+'/opp.log', format='%(asctime)s [%(levelname)-5s] %(message)s',level=log_level)

    logging.info('starting worker...')
    logging.info('log level is %s' % str_log_level)

    logging.info("KILL TIME is %s." % str_kill_start_time)

    str_now = datetime.now().strftime('%H:%M')

    if str_now >= str_kill_start_time and str_now <= str_kill_end_time:
        logging.info('searching shutdown process.')

        proc_list = []
        for proc in psutil.process_iter():
            if proc.name() in ['OTMS.exe','OTService.exe']:
                #f.write("STEP2-1\n")
                logging.info("%s is found." % (proc))
                proc.kill()
                logging.info("%s is killed." % (proc))
    else:
        logging.info('Wait until kill time.')

    #f.write("STEP3\n")
    logging.info('done')
    #f.close()
    return True


if __name__ == '__main__':

    check_otms()


